const { Router } = require("express");

const { MovieService } = require("../services/MovieService");

const moviesController = Router();

moviesController.get("/", async (req, res, next) => {
  try {
    const movies = await MovieService.findMovies();
    res.status(200).json(movies);
  } catch (err) {
    next(err);
  }
});

moviesController.get("/:id", async (req, res, next) => {
  try {
    const movie = await MovieService.findMovieById(req.params.id);
    res.status(200).json({ movie });
  } catch (err) {
    next(err);
  }
});

module.exports = { moviesController };
