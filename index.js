const express = require("express");
const cors = require("cors");

const { moviesController } = require("./controllers/movies");

const PORT = 3000;

const app = express();
app.use(cors());
app.use(express.json());

app.use("/movies", moviesController);

app.listen(PORT, () => {
  console.log(`Server listening on localhost:${PORT}`);
});
