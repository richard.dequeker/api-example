const errorHandler = (err, req, res, next) => {
  if (!err) {
    next();
  }
  switch (err.message) {
    case "Not Found":
      res.status(404).json({ message: "Not Found" });
      break;
    default:
      res.status(400).json({ message: "Bad request" });
  }
};
