const Sequelize = require("sequelize");
const { sequelize } = require("../connection");

const Movie = sequelize.define(
  "movies",
  {
    title: Sequelize.STRING,
    cover: Sequelize.STRING,
    description: Sequelize.STRING,
  },
  {}
);

sequelize.sync({ force: true });

module.exports = { Movie };
