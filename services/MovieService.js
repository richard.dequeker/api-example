const { Movie } = require("../models/Movie");

class MovieService {
  static async findMovies() {
    return await Movie.findAll();
  }

  static async findMovieById(id) {
    const movie = await Movie.findOne({ where: { id } });
    if (!movie) {
      throw new Error("Not Found");
    }
    return movie;
  }
}

module.exports = { MovieService };
